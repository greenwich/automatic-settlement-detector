import React, { Component } from 'react';
import {Map, Marker, InfoWindow, GoogleApiWrapper} from 'google-maps-react';
import axios from 'axios';




export class MapContainer extends Component {
  state = {
    showingInfoWindow: false,
    activeMarker: {},
    selectedPlace: {},
    syriaDataset: [],
  };

  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });

  onMapClicked = (props) => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      })
    }
  };



 componentDidMount () {
    axios.get('http://poselenie.surge.sh/syria_settles.json').then( response => {
      const syriaDataset = response.data.features
      this.setState({ syriaDataset })
    console.log(`cons syr - ${this.state.syriaDataset.name}`)
    console.log(`cons feat - ${this.state.syriaDataset[0].properties.Latitude}`)
    console.log(`cons feat - ${this.state.syriaDataset[0].properties.Longitude}`)
    })

  }
  render() {
  
  // hardcoded data For Polygon
  // const triangleCoords = [{lng: 32.0529428, lat: 3.5921812}, {lng: 32.0870665, lat: 3.5604119}, {lng: 32.0860312, lat: 3.531521}, {lng: 32.2008436, lat: 3.5071038}, {lng: 32.2178889, lat: 3.5637122}, {lng: 32.1971656, lat: 3.6004917}, {lng: 32.4152796, lat: 3.745619}, {lng: 32.7202344, lat: 3.767808}, {lng: 32.9011652, lat: 3.8156061}, {lng: 33.0277605, lat: 3.8936137}, {lng: 33.1788214, lat: 3.7787452}, {lng: 33.5110748, lat: 3.7518217}, {lng: 33.9877616, lat: 4.2340766}, {lng: 34.065686, lat: 4.1440089}, {lng: 34.0495316, lat: 4.1211187}, {lng: 34.0913561, lat: 4.0490804}, {lng: 34.0588355, lat: 4.0276953}, {lng: 34.0600522, lat: 4.0095127}, {lng: 34.1346408, lat: 3.9613466}, {lng: 34.119928, lat: 3.9008365}, {lng: 34.0914395, lat: 3.8852439}, {lng: 34.0884347, lat: 3.8612687}, {lng: 34.2155716, lat: 3.8808589}, {lng: 34.2260109, lat: 3.8310067}, {lng: 34.1800615, lat: 3.8315599}, {lng: 34.1531728, lat: 3.8069669}, {lng: 34.1886466, lat: 3.7951329}, {lng: 34.1708766, lat: 3.7677056}, {lng: 34.2455846, lat: 3.7838912}, {lng: 34.3079276, lat: 3.7113883}, {lng: 34.3310228, lat: 3.7330424}, {lng: 34.3650724, lat: 3.7351606}, {lng: 34.4049071, lat: 3.6927435}, {lng: 34.463224, lat: 3.6694964}, {lng: 34.4514969, lat: 3.5171323}, {lng: 34.4247746, lat: 3.4917143}, {lng: 34.3889712, lat: 3.4917681}, {lng: 34.4166072, lat: 3.4506569}, {lng: 34.4006166, lat: 3.3705844}, {lng: 34.4299582, lat: 3.3427126}, {lng: 34.4558221, lat: 3.181988}, {lng: 34.4948402, lat: 3.1413517}, {lng: 34.5458158, lat: 3.1365292}, {lng: 34.5723007, lat: 3.091398}, {lng: 34.5990399, lat: 2.9246764}, {lng: 34.6392915, lat: 2.9067303}, {lng: 34.6528953, lat: 2.8675618}, {lng: 34.6923843, lat: 2.8628737}, {lng: 34.7017464, lat: 2.8776814}, {lng: 34.736348, lat: 2.8549855}, {lng: 34.7753794, lat: 2.7877923}, {lng: 34.7754109, lat: 2.6978801}, {lng: 34.7991898, lat: 2.6558329}, {lng: 34.8682472, lat: 2.5705875}, {lng: 34.8938648, lat: 2.5928566}, {lng: 34.9129964, lat: 2.5165849}, {lng: 34.9330601, lat: 2.5159403}, {lng: 34.9544121, lat: 2.4724079}, {lng: 34.9143475, lat: 2.402942}, {lng: 34.9845547, lat: 1.9851674}, {lng: 35.0002053, lat: 1.9624087}, {lng: 34.9915182, lat: 1.6663205}, {lng: 34.9436701, lat: 1.5766085}, {lng: 34.8640774, lat: 1.5283409}, {lng: 34.8442388, lat: 1.457112}, {lng: 34.7938485, lat: 1.4135757}, {lng: 34.8061323, lat: 1.3829384}, {lng: 34.7870951, lat: 1.3652242}, {lng: 34.7780851, lat: 1.3932315}, {lng: 34.7189677, lat: 1.4423116}, {lng: 34.7261814, lat: 1.4696888}, {lng: 34.6991964, lat: 1.5269232}, {lng: 34.6584943, lat: 1.5263945}, {lng: 34.6212363, lat: 1.5757469}, {lng: 34.5925398, lat: 1.5873904}, {lng: 34.4943333, lat: 1.6046299}, {lng: 34.418406, lat: 1.5555548}, {lng: 34.3468117, lat: 1.5705259}, {lng: 34.3273221, lat: 1.5536879}, {lng: 34.2765592, lat: 1.549215}, {lng: 34.2288256, lat: 1.5642674}, {lng: 34.21028, lat: 2.1218962}, {lng: 33.732825, lat: 2.4189388}, {lng: 33.672036, lat: 2.3575806}, {lng: 33.6311186, lat: 2.3604224}, {lng: 33.6108986, lat: 2.3364398}, {lng: 33.5818647, lat: 2.3296491}, {lng: 33.535264, lat: 2.3594949}, {lng: 33.545557, lat: 2.2833353}, {lng: 33.4904801, lat: 2.2322587}, {lng: 33.4404576, lat: 2.2437913}, {lng: 33.4779301, lat: 2.1989251}, {lng: 33.454394, lat: 2.1303299}, {lng: 33.4367891, lat: 2.1137792}, {lng: 33.4151885, lat: 2.1455942}, {lng: 33.3675666, lat: 2.1104769}, {lng: 33.3467997, lat: 2.0662418}, {lng: 33.3632882, lat: 2.0305838}, {lng: 33.3260239, lat: 2.0550188}, {lng: 33.2936654, lat: 2.0455166}, {lng: 33.270713, lat: 1.9598537}, {lng: 33.2480116, lat: 1.9540309}, {lng: 33.2413942, lat: 1.9265829}, {lng: 33.141035, lat: 1.8597445}, {lng: 33.1131653, lat: 1.8565348}, {lng: 33.083081, lat: 1.7345981}, {lng: 33.0460603, lat: 1.710778}, {lng: 32.9992007, lat: 1.7150036}, {lng: 32.9957129, lat: 1.6742006}, {lng: 32.9444869, lat: 1.6447058}, {lng: 32.9654331, lat: 1.6182251}, {lng: 32.9729423, lat: 1.4799109}, {lng: 32.8353933, lat: 1.480122}, {lng: 32.6437543, lat: 1.4391523}, {lng: 32.6193416, lat: 1.4449294}, {lng: 32.5617667, lat: 1.5179281}, {lng: 32.5097436, lat: 1.5243583}, {lng: 32.409927, lat: 1.5931066}, {lng: 32.3243305, lat: 1.6225902}, {lng: 32.2839076, lat: 1.6628966}, {lng: 32.1973463, lat: 1.6779976}, {lng: 32.0932122, lat: 1.6526861}, {lng: 32.1064755, lat: 1.7419232}, {lng: 32.1655563, lat: 1.779619}, {lng: 32.2712006, lat: 1.8094361}, {lng: 32.2782233, lat: 1.8398171}, {lng: 32.3423587, lat: 1.8611739}, {lng: 32.3416118, lat: 1.926065}, {lng: 32.3629055, lat: 1.952417}, {lng: 32.332818, lat: 1.9679021}, {lng: 32.3520239, lat: 2.0771476}, {lng: 32.318046, lat: 2.1433087}, {lng: 32.316138, lat: 2.1975509}, {lng: 32.2601782, lat: 2.2564072}, {lng: 32.2221313, lat: 2.2490959}, {lng: 32.188488, lat: 2.2197224}, {lng: 32.1477505, lat: 2.2242676}, {lng: 32.0525134, lat: 2.285224}, {lng: 32.0248939, lat: 2.2792442}, {lng: 32.0123643, lat: 2.3091954}, {lng: 31.9413473, lat: 2.3415454}, {lng: 31.9177137, lat: 2.3682659}, {lng: 31.8324863, lat: 2.3719049}, {lng: 31.7413315, lat: 2.2977785}, {lng: 31.6988175, lat: 2.2911964}, {lng: 31.6466776, lat: 2.25856}, {lng: 31.5689985, lat: 2.287004}, {lng: 31.4661347, lat: 2.2299176}, {lng: 31.3358136, lat: 2.2501408}, {lng: 31.3056758, lat: 2.1569066}, {lng: 31.2030752, lat: 2.2208559}, {lng: 31.2032741, lat: 2.2932141}, {lng: 31.1665144, lat: 2.2756643}, {lng: 31.1413023, lat: 2.28108}, {lng: 31.1254746, lat: 2.2640586}, {lng: 31.0700707, lat: 2.3028452}, {lng: 31.0854616, lat: 2.3093821}, {lng: 31.0743005, lat: 2.3436263}, {lng: 31.0079902, lat: 2.3984947}, {lng: 30.9544647, lat: 2.4005085}, {lng: 30.9417627, lat: 2.3437009}, {lng: 30.910563, lat: 2.3314941}, {lng: 30.8812063, lat: 2.3415668}, {lng: 30.8338259, lat: 2.4269826}, {lng: 30.7779286, lat: 2.438698}, {lng: 30.7516427, lat: 2.4196969}, {lng: 30.7400708, lat: 2.4541926}, {lng: 30.761814, lat: 2.5100708}, {lng: 30.7500889, lat: 2.5862771}, {lng: 30.7779628, lat: 2.6037019}, {lng: 30.822752, lat: 2.7497315}, {lng: 30.8592104, lat: 2.7736021}, {lng: 30.8901471, lat: 2.8582632}, {lng: 30.8574363, lat: 2.9282862}, {lng: 30.8580032, lat: 2.9616657}, {lng: 30.8225795, lat: 3.0041049}, {lng: 30.7807101, lat: 3.0226612}, {lng: 30.7679924, lat: 3.0519071}, {lng: 30.7943528, lat: 3.0691113}, {lng: 30.8382459, lat: 3.2632849}, {lng: 30.8706699, lat: 3.2804776}, {lng: 30.8745877, lat: 3.3055018}, {lng: 30.892562, lat: 3.3145722}, {lng: 30.8874766, lat: 3.3411505}, {lng: 30.9368053, lat: 3.3991669}, {lng: 30.9420202, lat: 3.5062436}, {lng: 30.8969867, lat: 3.503127}, {lng: 30.8695683, lat: 3.4780262}, {lng: 30.8488404, lat: 3.4966668}, {lng: 30.866058, lat: 3.493664}, {lng: 30.908435, lat: 3.59233}, {lng: 30.949044, lat: 3.633025}, {lng: 30.978641, lat: 3.697502}, {lng: 31.0297642, lat: 3.6993151}, {lng: 31.0685739, lat: 3.74508}, {lng: 31.1007903, lat: 3.7364859}, {lng: 31.1637497, lat: 3.7907762}, {lng: 31.2890642, lat: 3.796602}, {lng: 31.507898, lat: 3.6763186}, {lng: 31.501771, lat: 3.6439855}, {lng: 31.5177649, lat: 3.638164}, {lng: 31.5757496, lat: 3.6823574}, {lng: 31.7073645, lat: 3.7239919}, {lng: 31.8113474, lat: 3.8249451}, {lng: 31.8698057, lat: 3.7847463}, {lng: 31.9574509, lat: 3.6599029}, {lng: 31.9581183, lat: 3.5719366}, {lng: 32.0529428, lat: 3.5921812}];
  

    return (
		<Map google={this.props.google}
		    style={{width: '100%', height: '100%', position: 'relative'}}
		    className={'map'}
        onClick={this.onMapClicked}
		    zoom={7}
          initialCenter={{
            lng: 35.86,
            lat: 33.039
          }}
		    >


{

  this.state.syriaDataset.map((sy_settle,sy_id) => (
      <Marker
            key={sy_id}
            title={sy_settle.properties.ADM4}
            onClick={this.onMarkerClick}
            name={sy_settle.properties.Name}
            water={60+sy_id}
            feed={100+sy_id}
            employee={400-sy_id}
            patron={'UN'}
            electricity={'mid level'}
            position={{lng: sy_settle.properties.Longitude, lat: sy_settle.properties.Latitude}} />
        

    )
  )

}
        <InfoWindow
          marker={this.state.activeMarker}
          visible={this.state.showingInfoWindow}>
            <div>
              <h2>Settle: {this.state.selectedPlace.name}</h2>
                <p>Drink water rations for { this.state.selectedPlace.water === undefined ? '(CRITICAL LEVEL!) 7' : `${this.state.selectedPlace.water} days`}</p>
                <p>Food rations for { this.state.selectedPlace.feed === undefined ? '(CRITICAL LEVEL!) 7' : `${this.state.selectedPlace.feed} days`}</p>
                <p>Settlers: { this.state.selectedPlace.employee === undefined ? 'no data': `${this.state.selectedPlace.employee} persons`}</p>
                <p>Patronage: { this.state.selectedPlace.water % 2 ? 'the settle has no care & charity': this.state.selectedPlace.patron }</p>
                <p>Electricity & phone service: { this.state.selectedPlace.water % 3 ? 'no': this.state.selectedPlace.electricity }</p>
            </div>
        </InfoWindow>
		</Map>
    );
  }
}

const LoadingContainer = (props) => (
  <div>Satellite connecting...</div>
)

 
export default GoogleApiWrapper({
  apiKey: 'AIzaSyAK1epcywT1PhU-O7vnQiQR3Mr-PclVP9M',
  LoadingContainer: LoadingContainer,
})(MapContainer)